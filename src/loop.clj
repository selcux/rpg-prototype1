(ns loop
  (:require
   [clojure.string :as string])
  (:import
   [clojure.lang IPersistentMap Keyword]))

(defn inspect [arg]
  (println "inspect" arg)
  arg)

(defn render-options
  "render-options function gets a list of maps as an argument
  where each map contains a :text key with a string value
  and returns the text value of each map with the indices of the list with a newline."
  [options]
  (some->> options
           (map-indexed (fn [i option] (str i ": " (:text option))))
           (string/join (System/lineSeparator))))

(defn render-event
  "render-event function gets a map as an argument
  where the map contains an :event key with a string value
  and :options key with a list of maps as an argument
  and returns the event value with the options value as a string."
  [event]
  (let [options (render-options (:options event))]
    (as-> (:info event) x
      (if (some? options)
        (str x (System/lineSeparator) options)
        x))))

(defn render
  "render function matches the position argument which contains one of the three values: :pre :post :in
  and renders the event if the given position matches the :render key in the state.
  If the position argument is not provided, it is assumed to be :in."
  [state position]
  (let [render-precedence (get-in state [:event :render])]
    (when (or (= render-precedence position)
              (and (nil? render-precedence) (= position :in)))
      (-> state
          (:event)
          (render-event)
          (println)))))

(defn update-event-state
  "Update state according to the given event id from the events map
  and return the updated state."
  [^IPersistentMap state ^Keyword id ^IPersistentMap events]
  (let [event (get events id)]
    (-> state
        (assoc :id id)
        (assoc :event event))))

(comment
  (def opts '({:text "foo"} {:text "bar"} {:text "baz"}))
  (println (render-options opts))
  (println (render-event {:info "infoo" :options opts}))

  (def state {:id :bar :event {:info "foo" :options opts :render :pre}})
  (get-in state [:event :info])

  (render state :in)
  (assoc state :id :foo)
  (str "---" "foo" nil))
