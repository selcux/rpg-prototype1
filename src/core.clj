(ns core
  (:require [input]
            [loop]
            [combat]
            [clojure.string :as s]))

(def game-state (atom {}))

(defn game-loop
  "The main game loop. This function will be called recursively until the game is over."
  [state events]
  (loop []
    (loop/render @state :pre)

    ;; if the current event contains the key :next in the :event of the state,
    ;; update the current state. Otherwise, keep it.
    (when-let [next-event-id (get-in @state [:event :next])]
      (swap! state loop/update-event-state next-event-id events))

    (cond
      (get-in @state [:event :combat]) (combat/play-turn state)

      :else (do
              (loop/render @state :in)
              (flush)

              (let [event-id (try
                               (input/read-player-choice (get-in @state [:event :options]))

                               (catch Exception _e
                                 (println)
                                 (println "Invalid input. Please try again.")
                                 (println)
                                 (:id @state)))]
                (swap! state loop/update-event-state event-id events)

                (combat/init-combat state))))

    (loop/render @state :post)

    (when (not= :game-over (:id @state))
      (recur))))

(defn -main []
  (let [events input/read-events]
    (swap! game-state loop/update-event-state :begin events)

    (game-loop game-state events)))

(comment
  (def opts {:options [{:text "foo"} {:text "bar"} {:text "baz"}]}))
