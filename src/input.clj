(ns input
  (:require [clojure.edn :as edn]
            [clojure.string :as string])
  (:import [clojure.lang IPersistentList]))

(def read-events
  "Retrieve events map from the file."
  (edn/read-string (slurp "assets/event-flow.edn")))

(defn read-line-int
  "Read a line from the console and return it as an integer."
  []
  (-> (read-line)
      (string/trim)
      (Integer/parseInt)))

(defn read-player-choice
  "Read the player input made for one of the options
  and return the :next value which is the event id."
  [^IPersistentList options]
  (let [player-choice (read-line-int)]
    (-> options
        (nth player-choice)
        (:next))))

(def read-player-attributes
  "Read the player attributes from the file."
  (edn/read-string (slurp "assets/player.edn")))