(ns combat
  (:require [clojure.string :as string]
            [input]
            [loop]))

(defn prepare-actions
  "Prepare the state for tne current turn,
   and return the turn actions with the state."
  [state]
  (-> ; Get the vector of actions inside the @state
   (get-in @state [:combat :player :actions])
   ; Append additional actions to the vector such as :end-turn
   (conj :end-turn)))

(defn render-actions
  "Render the current actions."
  [actions]
  (->> actions
       ; Convert each item from a keyword to a string
       (map name)
       ; Map each action to a string with the index and the text
       (map-indexed (fn [i action] (str i ": " action)))
       ; Join the strings with a newline
       (string/join (System/lineSeparator))
       (println))
  actions)

(defmulti play-turn
  "Play a turn of the combat phase."
  (fn [state] (get-in @state [:combat :turn])))

(defmethod play-turn :player
  [state]
  (println "Player's turn...")

  (let* [_actions (-> state
                      (prepare-actions)
                      (render-actions))
         player-choice (input/read-line-int)]

        (println "Player choice:" player-choice)))

(defmethod play-turn :enemy
  [_state]
  (println "Enemy's turn"))

(defn init-combat
  "Initialize the combat phase."
  [state]
  (when-some [_ (get-in @state [:event :combat])]
    (let* [player input/read-player-attributes
           combat-state {:turn :player
                         :player player
                         :enemy (get-in @state [:event :enemy])}]
          (swap! state assoc :combat combat-state))))

(comment
  (def combat-state (atom {:combat {:turn :enemy
                                    :player {:stats {:hp 78
                                                     :attack 8}
                                             :actions [:attack]}
                                    :enemy {:stats {:hp 10
                                                    :attack 5}
                                            :actions [:attack]}}}))

  (play-turn {:combat {:turn :player}})
  (play-turn {:combat {:turn :enemy}})
  (-> (prepare-actions combat-state)
      (render-actions)))